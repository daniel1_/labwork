#include <iostream>

int getAnIntFromUser() {
	int number;

	std::cout << "Input an integer" << std::endl;
	std::cin >> number;

	return number;
}

void get5IntFromUser() {
	int inputArr[5], sum = 0;

	std::cout << "Input five integers" << std::endl;
	
	int n = 0;
	while (n < 5) {
		std::cout << "Enter input " << n+1 << ":" << std::endl;
		std::cin >> inputArr[n];
		// std::cout << inputArr[n];
		sum = sum + inputArr[n];
		++n;
	}

	std::cout << "Your median is :" << inputArr[2] << std::endl;
	std::cout << "Your mean is :" << sum / 5 << std::endl;

}

int main() {
	// std::cout << "My name is Daniel Unwana" << std::endl;

	get5IntFromUser();

	int number = getAnIntFromUser();

	int len = (number % 2 == 0) ? 20 : 30;

	for (int i = 1; i <=len; ++i) {
		if (number % 2 == 0) {
			std::cout << i << " x " << number << " :" << number * i << std::endl;
		} else {
			if (i % number > 0){
				std::cout << i << std::endl;
			}
		}
	}
}
